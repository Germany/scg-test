<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
use Zend\Cache\PatternFactory;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestController extends AbstractActionController
{

    private $cache;

    public function __construct()
    {
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);

        $this->cache = StorageFactory::factory([
            'adapter' => [
                'name' => 'filesystem'
            ],
            'plugins' => [
                // Don't throw exceptions on cache errors
                'exception_handler' => [
                    'throw_exceptions' => false
                ],
            ],
        ]);

    }

    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->lang = $this->params()->fromRoute('lang', 'th');
        $view->action = $this->params()->fromRoute('action', 'index');
        $view->id = $this->params()->fromRoute('id', '');
        $view->page = $this->params()->fromQuery('page', 1);
        return $view;       
    } 

    /**
     * Test Index
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction(): object 
    {
        try
        {
            $view = $this->basic();
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }

    /**
     * Load question by id 
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function loadQuestionAction(): object
    {
        // Simple validate for testing only
        $id = $this->params()->fromQuery('id', 1);

        // Question data for test
        $questions = [
            '1' => [
                'id' => 1,
                'name' => '3, 5, 9, 15, X - Please create new function for finding X value'
            ],
            '2' => [
                'id' => 2,
                'name' => '(Y + 24)+(10 × 2) = 99 - Please create new function for finding Y value'
            ],
            '3' => [
                'id' => 3,
                'name' => 'If 1 = 5 , 2 = 25 , 3 = 325 , 4 = 4325 Then 5 = X - Please create new function for finding X value'
            ]
        ];

        return new JsonModel($questions[$id]);
    }
    
    /**
     * Find result by id
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function findResultAction(): object
    {
        // Simple validate for testing only
        $id = intval($this->params()->fromQuery('id', 1));

        if ($id == 1) {
            $result = $this->findX(5);
            $result['result'] = 'X = '.$result['result'];
        } elseif ($id == 2) {
            $result = $this->findY(99);
            $result['result'] = 'Y = '.$result['result'];
        } elseif ($id == 3) {
            $result = $this->findXLoop(5);
            $result['result'] = 'X = '.$result['result'];
        }

        return new JsonModel($result);
    }

    /**
     * PHP Test 1
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function test1Action(): object
    {
        $view = $this->basic();
        $position = intval($this->params()->fromQuery('position'));

        if(empty($position)){
            $position = 5;
        }

        $view->position = $position;
        $view->answer = $this->findX($position);
        
        return $view;
    }

    /**
     * PHP Test 2
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function test2Action(): object
    {
        $view = $this->basic();
        $total = intval($this->params()->fromQuery('total'));

        if(empty($total)){
            $total = 99;
        }

        $view->total = $total;
        $view->answer = $this->findY($total);

        return $view;
    }

    /**
     * PHP Test 3
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function test3Action(): object
    {
        $view = $this->basic();
        $position = intval($this->params()->fromQuery('position'));

        if(empty($position)){
            $position = 5;
        }

        $view->position = $position;
        $view->answer = $this->findXLoop($position);

        return $view;
    }

    /**
     * Find x position
     *
     * @param int $position
     * @return array
     */
    private function findX($position = 5): array
    {
        // Check Caching
        $cacheKey = 'findX-'.$position;
        $data = json_decode($this->cache->getItem($cacheKey, $success), true);

        if (!$success) {
            $start = 3;
            $step = 2;
            $sumStep = 0;
            $allStep = [];

            for ($i=0; $i < $position ; $i++) {
                $allStep[] = "$start";

                if ($position == $i+1) {
                    $result = $start;
                }

                $sumStep += $step;
                $start += $sumStep;
            }

            $question = $allStep;
            array_pop($question);
            $question[] = 'X';

            $data = [
                'result' => $result,
                'text' => implode($allStep, ', '),
                'question' => implode($question, ', ')
            ];
            $this->cache->setItem($cacheKey, json_encode($data));
        }

        $data['cache'] = $success;

        return $data;
    }

    /**
     * Find Y value
     *
     * @param int $total
     * @return array
     */
    private function findY($total = 99): array
    {
        // Check Caching
        $cacheKey = 'findY-'.$total;
        $data = json_decode($this->cache->getItem($cacheKey, $success), true);

        if (!$success) {

            // Equation (Y+24)+(10x2) = $total(99)
            $leftSum = 24+(10*2);
            $result = $total - $leftSum;
            $data = [
                'result' => $result,
                'text' => "($result + 24)+(10 × 2) = $total",
                'question' => "(Y + 24)+(10 × 2) = $total"
            ];
            $this->cache->setItem($cacheKey, json_encode($data));
        }

        $data['cache'] = $success;

        return $data;
    }

    /**
     * Find X Loop value
     *
     * @param int $loop
     * @return array
     */
    private function findXLoop($position = 5): array
    {
        // Check Caching
        $cacheKey = 'findXLoop-'.$position;
        $data = json_decode($this->cache->getItem($cacheKey, $success), true);

        if (!$success) {

            $allStep = [];
            $start = 5;
            $step = '';
            for ($i=0; $i < $position; $i++) { 
    
                if ($i == 0) {
                    $step = $start;
                } else {
                    $step =  ($i+1).$step;
                }
    
                if ($position == $i+1) {
                    $result = $step;
                }
    
                $allStep[] = ($i+1) .' = '.$step;
            }
    
            $question = $allStep;
            array_pop($question);
            $question[] = $i. ' = X';
    
            $data = [
                'result' => $result,
                'text' => implode($allStep, ', '),
                'question' => implode($question, ', ')
            ];
            $this->cache->setItem($cacheKey, json_encode($data));
        }

        $data['cache'] = $success;

        return $data;
    }
}
