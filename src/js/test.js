import Question from './components/Question.vue'

const app = new Vue({
    el: '#app',
    components: {
        'question': Question
    },
	data: {
		mode: 'vue'
	},
	methods: {
        selectMode: function (mode){
            this.mode = mode;
        }
	},
	mounted() {
        
	}
});
